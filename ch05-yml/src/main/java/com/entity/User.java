package com.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;
import java.util.Map;
import java.util.Properties;

@ConfigurationProperties(prefix = "my")
public class User {
    private Properties properties = new Properties();
    private int code;
    private Address addr;

    private String myName;

    private String port;
    //list不需要实例化，spring boot帮我们实例化一个list接口的实现类
    //作业（TODO）：实现类名字是什么？？
    private List<String> aiHao;

    private Map<String,Integer> scores;

    public Map<String, Integer> getScores() {
        return scores;
    }

    public void setScores(Map<String, Integer> scores) {
        this.scores = scores;
    }

    public List<String> getAiHao() {
        return aiHao;
    }

    public void setAiHao(List<String> aiHao) {
        this.aiHao = aiHao;
    }

    public String getMyName() {
        return myName;
    }

    public void setMyName(String myName) {
        this.myName = myName;
    }


    public String getLastName() {
        return (String) this.properties.get("lastName");
    }

    public void setLastName(String lastName) {
        //this.myName = myName;
        properties.put("lastName",lastName);

    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Address getAddr() {
        return addr;
    }

    public void setAddr(Address addr) {
        this.addr = addr;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
