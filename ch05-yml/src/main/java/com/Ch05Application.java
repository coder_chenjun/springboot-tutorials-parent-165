package com;

import com.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(User.class)
public class Ch05Application implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(Ch05Application.class, args);
    }

    @Autowired
    private User user;
    @Override
    public void run(String... args) throws Exception {
        System.out.println("user.getMyName() = " + user.getMyName());
        System.out.println("user.getCode() = " + user.getCode());
        System.out.println("user.getLastName() = " + user.getLastName());
        user.getAiHao().forEach(System.out::println);

        System.out.println("user.getScores().get(\"yuwen\") = " + user.getScores().get("yuwen"));
        System.out.println("user.getAddr().getCity() = " + user.getAddr().getCity());

        //演示多配置文件的情况
        System.out.println("user.getPort() = " + user.getPort());
    }
}
