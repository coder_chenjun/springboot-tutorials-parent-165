package com.nf.ch02.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * 这个类里面每次保留一个bean方法来测试
 */
@Configuration
public class DruidConfiguration {

    /**
     * 把配置信息写死在代码里了，不推荐
     * @param config
     * @return
     */
 /*   @Bean
    public DataSource druidDataSource(){
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setUsername("root");
        druidDataSource.setPassword("root");
        druidDataSource.setUrl("jdbc:mysql://localhost:3306/demo");
        return druidDataSource;
    }*/

    /**
     * 使用自定义的配置属性类来获取数据库连接信息以创建druid dataSource对象
     * @param config
     * @return
     */
/*    @Bean
    public DataSource druidDataSource(DbConfig config){
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setUsername(config.getMyname());
        druidDataSource.setPassword(config.getPassword());
        druidDataSource.setUrl(config.getMyurl());
        return druidDataSource;
    }*/

    /**
     * 仍然使用jdbc starter中的配置属性类来创建druid dataSource
     * 这样就可以仍然使用配置文件中spring.datasource前缀配置的信息
     * @param config
     * @return
     */
    /*@Bean
    public DataSource druidDataSource(DataSourceProperties config){
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setUsername(config.getUsername());
        druidDataSource.setPassword(config.getPassword());
        druidDataSource.setUrl(config.getUrl());
        return druidDataSource;
    }*/

    /**
     * DataSourceBuilder来自于spring boot
     * 利用DataSourceBuilder这个方便的构建（builder）工具类来创建druid dataSource
     *
     * 这也是DataSourceBuilder的常规使用方法
     * @return
     */
/*    @Bean
    public DataSource druidDataSource(DataSourceProperties config){
        return DataSourceBuilder.create() //基本等价于new DataSourceBuilder
                .type(DruidDataSource.class)
                .url(config.getUrl())
                .username(config.getUsername())
                .password(config.getPassword())
                .build();
    }*/

    /**
     * 使用type方法指定创建的DataSource类型的方式直接构建dataSource
     *
     * 使用了配置文件中的配置信息来创建druid dataSource
     * 可以使用spring.datasource前缀，也可以自己指定一个前缀
     *
     * 这样能成功的前提就是配置文件中的配置项与DruidDataSource中的属性名要“兼容”
     * @return
     */
    @Bean
    @ConfigurationProperties("spring.datasource") //利用原本的前缀进行配置
    //@ConfigurationProperties("druid")//设置自定义的前缀进行配置
    public DataSource druidDataSource(){
        //下面的代码执行完时，创建的dataSource是没有相关属性值的，等价于直接new了一个DruidDataSource对象
        // 是靠@ConfigurationProperties注解的功能来完成对dataSource对象的赋值操作的
        return DataSourceBuilder
                .create()
                .type(DruidDataSource.class)
                .build();

    }


}
