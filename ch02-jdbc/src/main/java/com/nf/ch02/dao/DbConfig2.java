package com.nf.ch02.dao;

/**
 * 这个类是说明不能在上面添加ConfigurationProperties注解时怎么处理
 * 比如这个类的源码你不能改动（第三方），见Ch02Application中的处理方法
 */
public class DbConfig2 {
    private String driverClass;

    public String getDriverClass() {
        return driverClass;
    }

    public void setDriverClass(String driverClass) {
        this.driverClass = driverClass;
    }

    @Override
    public String toString() {
        return "DbConfig2{" +
                "driverClass='" + driverClass + '\'' +
                '}';
    }
}
