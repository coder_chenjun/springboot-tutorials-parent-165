package com.nf.ch02.dao;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "cj")
public class DbConfig {
    private String myurl;
    private String myname;
    private String password;

    public String getMyurl() {
        return myurl;
    }

    public void setMyurl(String myurl) {
        this.myurl = myurl;
    }

    public String getMyname() {
        return myname;
    }

    public void setMyname(String myname) {
        this.myname = myname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "DbConfig{" +
                "myurl='" + myurl + '\'' +
                ", myname='" + myname + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
