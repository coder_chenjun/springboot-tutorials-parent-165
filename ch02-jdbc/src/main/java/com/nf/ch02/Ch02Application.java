package com.nf.ch02;

import com.nf.ch02.dao.DbConfig;
import com.nf.ch02.dao.DbConfig2;
import com.nf.ch02.dao.UserDaoImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableConfigurationProperties({DbConfig.class})
public class Ch02Application {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Ch02Application.class, args);

        UserDaoImpl userDao = context.getBean(UserDaoImpl.class);
        userDao.demo();

        DataSourceProperties dataSourceProperties = context.getBean(DataSourceProperties.class);
        System.out.println("dataSourceProperties url= " + dataSourceProperties.getUrl());

        DbConfig config = context.getBean(DbConfig.class);
        System.out.println("config = " + config);

        DbConfig2 config2 = context.getBean(DbConfig2.class);
        System.out.println("config2 = " + config2);
    }


    /**
     * 1.像创建普通的bean对象一样创建配置属性类bean对象
     * 2.在上面添加@ConfigurationProperties注解并指定前缀即可
     * 3.“不能”在EnableConfigurationProperties里添加DbConfig2.class
     *  比如@EnableConfigurationProperties({DbConfig.class,DbConfig2.class})
     *  会报No ConfigurationProperties annotation found on  'com.nf.ch02.dao.DbConfig2'.的错误
     * @return
     */
    @Bean
    @ConfigurationProperties(prefix = "db2")
    public DbConfig2 dbConfig2(){
        return new DbConfig2();
    }
}
