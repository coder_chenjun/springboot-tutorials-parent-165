package com;


import com.service.EsRestTemplateService;
import com.service.RepositoryServiceImpl;
import com.service.RestHighLevelClientServiceImpl;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

//Spring Boot整合的时候可以不添加EnableElasticsearchRepositories
@SpringBootApplication
public class Ch09Application implements CommandLineRunner {
    public static void main(String[] args) {

        SpringApplication.run(Ch09Application.class, args);
    }
    @Autowired
    private BeanFactory beanFactory;

    @Autowired
    private RestHighLevelClientServiceImpl restHighLevelClientService;

    @Autowired
    private EsRestTemplateService esRestTemplateService;

    @Autowired
    private RepositoryServiceImpl repositoryService;

    @Override
    public void run(String... args) throws Exception {

        List<String> strings = AutoConfigurationPackages.get(this.beanFactory);
        strings.forEach(System.out::println);
        // restHighLevelClientService.doSth();
       // esRestTemplateService.doSth();
        //repositoryService.doSth();
    }
}
