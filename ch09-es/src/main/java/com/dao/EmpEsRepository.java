package com.dao;


import com.entity.Emp;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


public interface EmpEsRepository extends ElasticsearchRepository<Emp,Integer> {
}
