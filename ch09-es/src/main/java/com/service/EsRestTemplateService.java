package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.stereotype.Service;

@Service
public class EsRestTemplateService {
    @Autowired
    private ElasticsearchRestTemplate restTemplate;
    public void doSth(){
        System.out.println("restTemplate = " + restTemplate);
    }
}
