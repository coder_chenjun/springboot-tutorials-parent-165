package com.service;

import com.dao.EmpEsRepository;
import com.entity.Emp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RepositoryServiceImpl {

    @Autowired
    private EmpEsRepository empEsRepository;

    public void doSth(){
        Iterable<Emp> emps = empEsRepository.findAll();

        for (Emp emp : emps) {
            System.out.println("emp = " + emp);
        }
    }
}
