package com.service;

import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RestHighLevelClientServiceImpl {
    
    @Autowired
    private RestHighLevelClient highLevelClient;
    
    public void doSth(){
        System.out.println("highLevelClient = " + highLevelClient);
        //通过高级别的客户端来获取低级别
        RestClient lowLevelClient = highLevelClient.getLowLevelClient();
        System.out.println("lowLevelClient = " + lowLevelClient);
    }
}
