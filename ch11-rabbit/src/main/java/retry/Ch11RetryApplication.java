package retry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import retry.service.RetryPublisherServiceImpl;

import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class Ch11RetryApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(Ch11RetryApplication.class, args);
    }

    @Autowired
    private RetryPublisherServiceImpl publisherService;
    @Override
    public void run(String... args) throws Exception {
        //TimeUnit.SECONDS.sleep(10);
        publisherService.sendMsgConfirmCallback();
    }
}
