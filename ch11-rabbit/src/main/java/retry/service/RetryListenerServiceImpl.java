package retry.service;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import retry.config.RetryConfiguration;

import java.io.IOException;

@Service
public class RetryListenerServiceImpl {



  /**
   *  auto：表示⾃动确认，⾃动确认会根据消费端在处理消息的过程是否抛出异常来决定返回ack或者nack给broker。如
   *   果消费成功则返回ack， broker接收到ack后⾃动从队列中移除此消息。如果消费过程抛出了异常导则返回nack，此
   *   时broker会根据default-requeue-rejected的设置决定是否将消息重新放回队列中。
   *
   *
   * 注意： 重试并不是 RabbitMQ 重新发送了消息到了队列，仅仅是消费者内部进行了重试，换句话说就是重试跟mq没有任何关系。
   *  下面消费者代码不能将异常给处理了，一旦捕获了异常，在自动 ack 模式下，就相当于消息正确处理了，消息直接被确认掉了，不会触发重试的。
   *
   */
    @RabbitListener(queues = RetryConfiguration.QUEUE_NAME)
    public void receiveMsgAuto(String msg) {
        System.out.println("接收到消息auto--msg = " + msg);
        throw new RuntimeException("runtime ex");
    }


}
