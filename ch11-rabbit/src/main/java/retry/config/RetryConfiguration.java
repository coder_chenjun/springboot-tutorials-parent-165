package retry.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RetryConfiguration {
    public static final String QUEUE_NAME = "queue_retry";
    @Bean
    public Queue ackQueue(){
        return QueueBuilder.durable(QUEUE_NAME).build();
    }

}
