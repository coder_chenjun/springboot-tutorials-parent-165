package ack;

import ack.service.AckPublisherServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch11AckApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(Ch11AckApplication.class, args);
    }

    @Autowired
    private AckPublisherServiceImpl ackPublisherService;

    @Override
    public void run(String... args) throws Exception {

        ackPublisherService.sendMsg();
       // ackPublisherService.sendMsgReturnCallback();
    }
}
