package ack.config;

import org.springframework.amqp.core.*;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AckConfiguration {
    @Bean
    public Queue ackQueue(){
        return QueueBuilder.durable("ackqueue").build();
    }

   /* @Bean
    public DirectExchange ackExchange(){
        DirectExchange directExchange = new DirectExchange("ackex");
        return directExchange;
    }

    @Bean
    public Binding binding(){
        return BindingBuilder.bind(ackQueue()).to(ackExchange()).with("ackqueue");

    }*/
}
