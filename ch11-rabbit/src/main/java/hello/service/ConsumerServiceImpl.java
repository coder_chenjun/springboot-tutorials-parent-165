package hello.service;

import custom.entity.Emp;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * 一个RabbitListener注解会产生一个channel
 *
 * 实战时，如果你需要依据数据类型来接收消息，建议用一个RabblitListener +多个RabbitHandler
 * 不要使用多个RabbitListener修饰多个方法
 *
 * 如果你消息只有一种类型，那么就可以使用RabbitListener
 *
 * 现在这个案例的情况(实战不这样做,这里只是演示,用RabbitHandler处理接收类型的不同的情况,见custom包的同名类)：
 * 1.先发送字符串后发送emp对象，此类的代码顺序必须一致，
 * 也就是receive（Emp）在receive（string）后面
 * 这种情况不是好的实战做法
 */
@Service
public class ConsumerServiceImpl {


    @RabbitListener(queues = "hello")
    public void receive(String data){
        System.out.println("String data  = " + data);
    }

    @RabbitListener(queues = "hello")
    public void receive(Emp data){
        System.out.println("Emp data = " + data);
    }

}
