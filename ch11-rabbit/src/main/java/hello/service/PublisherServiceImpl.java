package hello.service;

import custom.entity.Emp;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
public class PublisherServiceImpl {
    private RabbitTemplate rabbitTemplate;

    //@Autowired //spring 4.2之后才有这个构造函数自动注入的能力
    public PublisherServiceImpl(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMsgString(){
        //下面的重载会自动指定交换机是“”（指的是默认交换机），
        // 路由key是“”
        //发送消息完整的信息是：交换机+路由key + 数据
        // 默认交换机是direct类型，绑定key是各个队列的名字
        rabbitTemplate.convertAndSend("hello","orderdata");

    }

    public void sendMsgEmp(){
        rabbitTemplate.convertAndSend("hello",new Emp(100,"abc"));
    }


    public void createOrder3(){
        //这里不能发送到队列，无法接收是因为默认交换机是direct类型的，只会发给routingkey完全匹配的队列
        rabbitTemplate.convertAndSend("quick.orange.rabbit","quick.orange.rabbit");
        //指定了交换机，routingkey符合2个绑定，会发给2个队列
        rabbitTemplate.convertAndSend("mytopicA","quick.orange.rabbit","quick.orange.rabbit-topic");

        rabbitTemplate.convertAndSend("mytopicA","quick.orange.rabbit",new Emp(100,"quick.orange.rabbit-topic emp"));
//        rabbitTemplate.convertAndSend("a.orange.b",new Emp(100,"a.orange.b"));
    }
}
