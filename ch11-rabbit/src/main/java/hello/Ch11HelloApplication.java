package hello;

import hello.service.PublisherServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch11HelloApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(Ch11HelloApplication.class, args);
    }

    @Autowired
    private PublisherServiceImpl publisherService;
    @Override
    public void run(String... args) throws Exception {
        publisherService.sendMsgString();
        publisherService.sendMsgEmp();
    }
}
