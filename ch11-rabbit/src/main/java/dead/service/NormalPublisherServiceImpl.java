package dead.service;

import dead.config.DeadConfiguration;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
public class NormalPublisherServiceImpl {
  @Autowired
  private RabbitTemplate rabbitTemplate;


  public void sendMsgConfirmCallback() {
    CorrelationData correlationData = new CorrelationData();
    String uuid = UUID.randomUUID()
            .toString();

    correlationData.setId(uuid);
    Object data = "msg:" + new Date();
    rabbitTemplate.convertAndSend(DeadConfiguration.NORMAL_EX_NAME,DeadConfiguration.NORMAL_ROUTING_KEY, data, correlationData);

    }



}
