package dead.service;

import dead.config.DeadConfiguration;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class DeadListenerServiceImpl {

    @RabbitListener(queues = DeadConfiguration.DEAD_QUEUE_NAME)
    public void receiveMsgAuto(String msg) {
        System.out.println("接收到死信xxxxxx = " + msg);
    }

}
