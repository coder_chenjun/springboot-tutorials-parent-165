package dead.service;

import dead.config.DeadConfiguration;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class NormalListenerServiceImpl {

    @RabbitListener(queues = DeadConfiguration.NORMAL_QUEUE_NAME)
    public void receiveMsgAuto(String msg) {
        System.out.println("接收到消息auto--msg = " + msg);
        throw new RuntimeException("runtime ex");
    }

}
