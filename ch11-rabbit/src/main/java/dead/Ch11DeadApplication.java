package dead;

import dead.service.NormalPublisherServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch11DeadApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(Ch11DeadApplication.class, args);
    }

    @Autowired
    private NormalPublisherServiceImpl publisherService;
    @Override
    public void run(String... args) throws Exception {
        publisherService.sendMsgConfirmCallback();
    }
}
