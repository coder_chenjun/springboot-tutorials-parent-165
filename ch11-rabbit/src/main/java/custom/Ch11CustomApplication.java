package custom;


import custom.service.PublisherServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch11CustomApplication implements CommandLineRunner {
  public static void main(String[] args) {
    SpringApplication.run(Ch11CustomApplication.class, args);
  }

  @Autowired
  private PublisherServiceImpl publisherService;
  @Override
  public void run(String... args) throws Exception {
    publisherService.sendMsgStringAndEmp();

  }
}
