package custom.service;

import custom.entity.Emp;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
public class PublisherServiceImpl {
  private RabbitTemplate rabbitTemplate;

  public PublisherServiceImpl(RabbitTemplate rabbitTemplate) {
    this.rabbitTemplate = rabbitTemplate;
  }


  public void sendMsgStringAndEmp() {
    //这里不能发送到队列，无法接收是因为默认交换机是direct类型的，只会发给routingkey完全匹配的队列
    rabbitTemplate.convertAndSend("quick.orange.rabbit", "quick.orange.rabbit");
    //指定了交换机，routingkey符合2个绑定，会发给2个队列
    rabbitTemplate.convertAndSend("mytopicA", "quick.orange.rabbit", "quick.orange.rabbit-topic");

    rabbitTemplate.convertAndSend("mytopicA", "quick.orange.rabbit", new Emp(100, "quick.orange.rabbit-topic emp"));
    //        rabbitTemplate.convertAndSend("a.orange.b",new Emp(100,"a.orange.b"));
  }
}
