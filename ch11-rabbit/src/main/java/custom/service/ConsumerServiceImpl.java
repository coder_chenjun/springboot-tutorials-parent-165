package custom.service;

import custom.entity.Emp;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
@RabbitListener(queues = "custom1")
public class ConsumerServiceImpl {

    @RabbitHandler
    public void receiveEmp(Emp data){
        System.out.println("Emp data = " + data);
    }

    @RabbitHandler
    public void receiveStr(String data){
        System.out.println("String data = " + data);
    }



}
