package delay;

import delay.service.DelayPublisherServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch11DelayApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(Ch11DelayApplication.class, args);
    }

    @Autowired
    private DelayPublisherServiceImpl publisherService;
    @Override
    public void run(String... args) throws Exception {
        publisherService.sendDelayedMsg();
    }
}
