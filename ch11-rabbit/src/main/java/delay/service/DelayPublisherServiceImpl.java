package delay.service;

import dead.config.DeadConfiguration;
import delay.config.DelayConfiguration;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
public class DelayPublisherServiceImpl {
    @Autowired
    private RabbitTemplate rabbitTemplate;


    public void sendDelayedMsg() {
        CorrelationData correlationData = new CorrelationData();
        String uuid = UUID.randomUUID().toString();

        correlationData.setId(uuid);
        Object data = "延迟消息文本";
        System.out.println("发布消息时间：----- " + new Date());
        rabbitTemplate.convertAndSend(DelayConfiguration.DELAY_EXCHANGE_NAME,
                DelayConfiguration.DELAY_ROUTING_NAME,
                data, message -> {
                    //调用setDelay方法就是给消息属性添加x-delay 头信息
                    //不指定这个头就当成普通消息处理，单位为毫秒
                    message.getMessageProperties().setDelay(10000);
                    return message;
                },
                correlationData);

    }


}
