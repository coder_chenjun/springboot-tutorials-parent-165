package delay.service;

import dead.config.DeadConfiguration;
import delay.config.DelayConfiguration;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class DelayListenerServiceImpl {

    @RabbitListener(queues = DelayConfiguration.DELAY_QUEUE_NAME)
    public void receiveMsgAuto(String msg) {
        System.out.println("接收消息时间：******" + new Date());
        System.out.println("接收到的消息 = " + msg);

    }

}
