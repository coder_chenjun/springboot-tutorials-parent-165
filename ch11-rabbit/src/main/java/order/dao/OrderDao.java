package order.dao;

import order.entity.Order;

public interface OrderDao {
    /**
     * 根据ID查询订单信息
     *
     * @param orderId
     * @return
     */
    Order getOrderById(String orderId);

    /**
     * 保存订单信息
     *
     * @param order
     */
    void saveOrder(Order order);

    /**
     * 修改订单
     *
     * @param order
     */
    void updateOrder(Order order);
}