package order.entity;

import lombok.Data;

@Data
public class Order {
    private String orderId;
    private Integer status;
    private String message;
}