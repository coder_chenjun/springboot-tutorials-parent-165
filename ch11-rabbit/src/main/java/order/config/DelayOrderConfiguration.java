package order.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.CustomExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * https://github.com/rabbitmq/rabbitmq-delayed-message-exchange (有此插件使用说明）
 * This plugin adds delayed-messaging (or scheduled-messaging) to RabbitMQ.
 *
 * A user can declare an exchange with the type x-delayed-message
 * and then publish messages with the custom header x-delay
 * expressing in milliseconds a delay time for the message.
 * The message will be delivered to the respective queues after x-delay milliseconds.
 */
@Configuration
public class DelayOrderConfiguration {
    public static final String DELAY_EXCHANGE_NAME = "delay_order_ex";
    public static final String DELAY_QUEUE_NAME = "delay_order_queue";
    public static final String DELAY_ROUTING_NAME = "delay_order_routing";

    @Bean
    public CustomExchange delayExchange(){
        Map<String, Object> params = new HashMap<>();
        params.put("x-delayed-type", "direct");
        return new CustomExchange(DELAY_EXCHANGE_NAME, "x-delayed-message", true, false, params);
    }

    @Bean
    public Queue delayQueue(){
        return new Queue(DELAY_QUEUE_NAME, true);
    }

    @Bean
    public Binding delayBinding(){
        return BindingBuilder
                .bind(delayQueue())
                .to(delayExchange())
                .with(DELAY_ROUTING_NAME)
                .noargs();
    }

    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }
}
