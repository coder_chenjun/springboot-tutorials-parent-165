package order;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("order.dao")
public class Ch11DelayOrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(Ch11DelayOrderApplication.class, args);
    }
}
