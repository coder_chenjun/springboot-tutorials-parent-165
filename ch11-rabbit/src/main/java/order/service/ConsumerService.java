package order.service;

import lombok.extern.slf4j.Slf4j;
import order.config.DelayOrderConfiguration;
import order.dao.OrderDao;
import order.entity.Order;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ConsumerService {
    /**
     * 注⼊OrderDao
     */
    @Autowired
    private OrderDao orderDao;

    /**
     * 接收消息
     * 这⾥会延迟接收，也就是在发送端指定的延迟时间后才才进⾏接收
     */
    @RabbitListener(queues = DelayOrderConfiguration.DELAY_QUEUE_NAME)
    public void receiveMessage(Order order) {
        log.info("接收消息,订单编号： " + order.getOrderId());
        //依据订单编号查询数据库，如果订单状态为1则将其更新为0，表示取消订单
        order = orderDao.getOrderById(order.getOrderId());
        if (order.getStatus() == 1) {
            order.setStatus(0);
            orderDao.updateOrder(order);
            log.info("订单已取消");
        }else{
            log.info("订单已经支付了 ^_^");
        }
    }
}