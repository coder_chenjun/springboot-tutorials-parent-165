package order.controller;

import order.entity.Order;
import order.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private ProducerService producerService;

    @RequestMapping("/create")
    public String createOrder(String id,String message){
        Order order = new Order();
        order.setOrderId(id);
        order.setMessage(message);
        order.setStatus(1);
        producerService.createOrder(order, 1000*60);
        return "已经创建订单，请在1分钟内支付成功";
    }

    @RequestMapping("/pay")
    public String createOrder(String id){
        Order order = producerService.getOrderById(id);
        // 2表示已经支付 0表示取消，1表示未支付
        order.setStatus(2);
        producerService.updateOrder(order);
        return "支付成功";
    }
}
