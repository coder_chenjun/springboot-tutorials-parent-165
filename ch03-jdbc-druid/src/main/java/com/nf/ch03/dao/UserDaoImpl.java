package com.nf.ch03.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void demo(){
        String sql = "select count(*) from dept";
        Integer count = jdbcTemplate.queryForObject(sql, Integer.class);
        System.out.println("count = " + count);
    }
}
