package com.nf.ch03;

import com.nf.ch03.dao.UserDaoImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Ch03Application {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Ch03Application.class, args);

        UserDaoImpl userDao = context.getBean(UserDaoImpl.class);
        userDao.demo();
    }
}
