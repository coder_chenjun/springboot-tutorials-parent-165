package com;

import com.dao.UserDao;
import com.entity.UserEntity;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;
import java.util.List;

/**
 * CommandLineRunner是程序启动时会自动执行其run方法一个回调机制
 */
@SpringBootApplication
@MapperScan("com.dao")
public class Ch04Application implements CommandLineRunner, ApplicationRunner {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(args));
        ConfigurableApplicationContext context = SpringApplication.run(Ch04Application.class, args);

//        UserDao userDao = context.getBean(UserDao.class);
//        UserEntity user = userDao.getById();
//        System.out.println("user = " + user);

    }

    @Autowired
    private UserDao userDao;

    /**
     *
     * @param args 来自于main方法的参数
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
       // System.out.println("run: " + Arrays.toString(args));
        UserEntity user = userDao.getById();
        System.out.println("user in run method userid=2 = " + user);

        List<UserEntity> pagedUsers = userDao.getPagedUsers(2, 2);
        pagedUsers.forEach(System.out::println);


    }

    /**
     * 在idea的program argumetn里面输入下面的内容
     * --开头的叫做选项参数
     * 其它就是非选项参数，空格分开
     * --d=dddd a b c
     * @param args
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        /*System.out.println("application runner run:");
        Set<String> optionNames = args.getOptionNames();
        for (String s : optionNames) {
            System.out.println("option:---" +s + "option value:" + args.getOptionValues(s));
        }

        List<String> stringList = args.getNonOptionArgs();
        for (String s : stringList) {
            System.out.println("non option:---" +s);
        }*/
    }
}
