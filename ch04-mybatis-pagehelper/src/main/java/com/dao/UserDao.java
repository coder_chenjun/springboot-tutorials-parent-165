package com.dao;

import com.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface UserDao {

  //  @Select("select id,emp_name as empName from employee where id =2 ")
    UserEntity getById();

  List<UserEntity> getPagedUsers(@Param("pageNum") int pageNum, @Param("pageSize")int pageSize);
}
