package com.controller;

import com.entity.Emp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Date;

@RestController
public class SessionDemoController {

    @Autowired
    private ServerProperties serverProperties;
    @RequestMapping("/w")
    public String writeSession(HttpSession session){
        System.out.println("---write:serverProperties.getPort() = " + serverProperties.getPort());
        System.out.println("session.getId() = " + session.getId());
        session.setAttribute("w",new Date());
        return "ok";
    }

    @RequestMapping("/token")
    public String getToken(HttpSession session){
        return  session.getId();
    }
    @RequestMapping("/r")
    public String readSession(HttpSession session){
        System.out.println("---read:serverProperties.getPort() = " + serverProperties.getPort());
        Object attribute = session.getAttribute("w");
        return attribute==null ? "no session":attribute.toString();
    }


    @RequestMapping("/w2")
    public String writeSession2(HttpSession session){
        System.out.println("---write:serverProperties.getPort() = " + serverProperties.getPort());
        session.setAttribute("w2",new Emp(100,"abc"));
        return "ok";
    }

    @RequestMapping("/r2")
    public String readSession2(HttpSession session){
        System.out.println("---read:serverProperties.getPort() = " + serverProperties.getPort());
        Object attribute = session.getAttribute("w2");
         return attribute==null ? "no session":attribute.toString();
    }
}
