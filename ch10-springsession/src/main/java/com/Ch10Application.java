package com;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@SpringBootApplication
public class Ch10Application implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(Ch10Application.class, args);
    }


    @Override
    public void run(String... args) throws Exception {

    }
}
