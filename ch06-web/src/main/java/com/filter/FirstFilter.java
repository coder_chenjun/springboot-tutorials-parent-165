package com.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * 添加WebFilter注解本来是可以注册成功的
 * 但spring boot由于使用的是内嵌的servlet容器，所以“不” 成功
 * 需要在任何一个配置类上面添加一个注解：@ServletComponentScan
 * 这个注解专门用在内嵌服务器上，并且主要扫描servlet，过滤器，servlet监听器
 * 此注解会扫描被它修饰的类所在的包里面的3大组件
 */
@WebFilter("/*")
public class FirstFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("-----servlet的过滤器，在mvc下一般用mvc拦截器而不用过滤器");

        chain.doFilter(request,response);
    }
}
