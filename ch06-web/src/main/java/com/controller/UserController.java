package com.controller;

import com.entity.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

@RestController
public class UserController {
    @RequestMapping("/")
    public User index(){

        return new User(100, "abc", new Date());
    }

    @RequestMapping("/upload")
    public void upload(MultipartFile file){
       // file.transferTo();
        System.out.println("upload");
    }

    //演示跨越的一个方法
    @RequestMapping("/data1")
    public void data1(){
        // file.transferTo();
        System.out.println("upload");
    }
}
