package com.service;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/*@Service
public class UserServiceImpl {
    //使用的类型是Slf4j:Simple Log Facade for Java
    private Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    public void doSth(){

        logger.trace("trace......非常详细的信息---");
        logger.debug("debug....");
        logger.info("info------进入");
        logger.warn("warn----");
        logger.error("error catch代码块");
    }
}*/


@Service
@Slf4j
public class UserServiceImpl {

    public void doSth(){

        log.trace("trace......非常详细的信息---");
        log.debug("debug....");
        log.info("info------进入");
        log.warn("warn----");
        log.error("error catch代码块");
    }
}