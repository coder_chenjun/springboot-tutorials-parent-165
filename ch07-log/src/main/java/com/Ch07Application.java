package com;

import com.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch07Application implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(Ch07Application.class, args);
    }

    @Autowired
    private UserServiceImpl userService;
    @Override
    public void run(String... args) throws Exception {
        userService.doSth();
    }
}
