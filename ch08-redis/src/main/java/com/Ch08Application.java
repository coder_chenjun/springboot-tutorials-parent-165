package com;

import com.service.RedisDemo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch08Application implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(Ch08Application.class, args);
    }

    @Autowired
    private RedisDemo redisDemo;
    @Override
    public void run(String... args) throws Exception {
        redisDemo.doSth();
    }
}
