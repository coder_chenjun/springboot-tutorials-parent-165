package com.config;

import org.springframework.boot.autoconfigure.data.redis.JedisClientConfigurationBuilderCustomizer;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;

import java.time.Duration;

public class MyJedisClientConfigurationCustomizer implements JedisClientConfigurationBuilderCustomizer {
    @Override
    public void customize(JedisClientConfiguration.JedisClientConfigurationBuilder clientConfigurationBuilder) {
//配置完毕之后就结束，不需要调用build方法
        clientConfigurationBuilder
                .clientName("asdfsads")
                .connectTimeout(Duration.ofSeconds(10));


    }
}
